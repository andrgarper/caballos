﻿using CaballosAGP.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CaballosAGP.Dtos;

namespace CaballosAGP.Data
{
    public class ControlContext : DbContext
    {

        public ControlContext(DbContextOptions<ControlContext> options) : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<Menu>().ToTable("Menu");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<RoleHasMenu>().ToTable("RoleHasMenu");
            modelBuilder.Entity<Admin>().ToTable("Admin");
            modelBuilder.Entity<Customer>().ToTable("Customer");
            modelBuilder.Entity<Teacher>().ToTable("Teacher");
            modelBuilder.Entity<Horse>().ToTable("Horse");
            modelBuilder.Entity<HorseClass>().ToTable("HorseClass");
            modelBuilder.Entity<HorseSaddle>().ToTable("HorseSaddle");
            modelBuilder.Entity<Menu>().ToTable("Menu");
            modelBuilder.Entity<RaceTrack>().ToTable("RaceTrack");         
            modelBuilder.Entity<UserAccount>().ToTable("UserAccount");



            /*modelBuilder.Entity<Admin>().HasOne(c => c.UserAccount).WithMany().HasForeignKey("UserAccountID").OnDelete(DeleteBehavior.Cascade);
           */
            /*
            //Carga Inicial de Menus
            modelBuilder.Entity<Menu>().HasData(
                new Menu
                {
                    ID=1,
                    Controller="UserAccounts",
                    Action="Index",
                    Label= "UserAccounts"
                },
                new Menu
                {
                    ID = 2,
                    Controller = "Roles",
                    Action = "Index",
                    Label = "Roles"
                },
                new Menu
                {
                    ID = 3,
                    Controller = "Menus",
                    Action = "Index",
                    Label = "Menus"
                },
                new Menu
                {
                    ID = 4,
                    Controller = "Horses",
                    Action = "Index",
                    Label = "Horses"
                },
                new Menu
                {
                    ID = 5,
                    Controller = "HorseSaddles",
                    Action = "Index",
                    Label = "HorseSaddles"
                },
                new Menu
                {
                    ID = 6,
                    Controller = "RaceTracks",
                    Action = "Index",
                    Label = "RaceTracks"
                },
                new Menu
                {
                    ID = 7,
                    Controller = "Bookings",
                    Action = "Index",
                    Label = "Bookings"
                },
                new Menu
                {
                    ID = 8,
                    Controller = "RoleHasMenus",
                    Action = "Index",
                    Label = "RoleHasMenus"
                },
                new Menu
                {
                    ID = 9,
                    Controller = "HorseClasses",
                    Action = "Index",
                    Label = "HorseClasses"
                },
                new Menu
                {
                    ID = 10,
                    Controller = "Customers",
                    Action = "Index",
                    Label = "Customers"
                },
                new Menu
                {
                    ID = 11,
                    Controller = "Teachers",
                    Action = "Index",
                    Label = "Teachers"
                },
                new Menu
                {
                    ID = 12,
                    Controller = "Admins",
                    Action = "Index",
                    Label = "Admins"
                });
           
            //Carga inicial de roles

            modelBuilder.Entity<Role>().HasData(
               new Role

               {
                   ID = 1,
                   RoleName = "Admin",
                   RoleDescription = "Rol de Admin",
                   Enabled = true
               },
                new Role

                {
                    ID = 2,
                    RoleName = "Teacher",
                    RoleDescription = "Rol de Teacher",
                    Enabled = true
                },
                 new Role

                 {
                     ID = 3,
                     RoleName = "Customer",
                     RoleDescription = "Rol de Customer",
                     Enabled = true
                    
                 });

            //Carga inicial de userAccount
            modelBuilder.Entity<UserAccount>().HasData(
                new UserAccount
                {
                    ID=1,
                    Username="admin",
                    Password= Util.Encriptar("1234"),
                    Email="admin@email.es",
                    Active=true,
                    RoleID=1
                },
                 new UserAccount
                 {
                     ID = 2,
                     Username = "teacher",
                     Password = Util.Encriptar("1234"),
                     Email = "teacher@email.es",
                     Active = true,
                     RoleID = 2
                 },
                  new UserAccount
                  {
                      ID = 3,
                      Username = "customer",
                      Password = Util.Encriptar("1234"),
                      Email = "customer@email.es",
                      Active = true,
                      RoleID = 3
                  }
                );
            modelBuilder.Entity<Admin>().HasData(
                new Admin
                {
                    ID=1,
                    Name="Admin",
                    Surname="Admin",
                    Age=30,
                    Phone="123456789",
                    UserAccountID=1

                }
                );
            modelBuilder.Entity<Teacher>().HasData(
                new Teacher
                {
                    ID = 1,
                    Name = "Teacher",
                    Surname = "Teacher",
                    Age = 35,
                    Phone = "123456789",
                    UserAccountID = 2

                }
                );
            modelBuilder.Entity<Customer>().HasData(
               new Customer
               {
                   ID = 1,
                   Name = "Customer",
                   Surname = "Customer",
                   Age = 40,
                   Phone = "123456789",
                   UserAccountID = 3,
                   Address="Calle de ejemplo",
                   Birthday=DateTime.Now,
                   City="Sevilla",
                   DNI="12345678V",
                   Level="1",
                   PostalCode="12345"
                   
                   

               }
               );
            //Carga inicial de roleHasMenu
            modelBuilder.Entity<RoleHasMenu>().HasData(
                new RoleHasMenu
                {
                    ID=1,
                    MenuID=1,
                    RoleID=1
                    
                },
                new RoleHasMenu
                {
                    ID = 2,
                    MenuID = 2,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 3,
                    MenuID = 3,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 4,
                    MenuID = 4,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 5,
                    MenuID = 5,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 6,
                    MenuID = 6,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 7,
                    MenuID = 7,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 8,
                    MenuID = 8,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 9,
                    MenuID = 9,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 10,
                    MenuID = 10,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 11,
                    MenuID = 11,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 12,
                    MenuID = 12,
                    RoleID = 1

                },
                new RoleHasMenu
                {
                    ID = 13,
                    MenuID = 4,
                    RoleID = 2

                },
                new RoleHasMenu
                {
                    ID = 14,
                    MenuID = 5,
                    RoleID = 2

                },
                new RoleHasMenu
                {
                    ID = 15,
                    MenuID = 6,
                    RoleID = 2

                },
                new RoleHasMenu
                {
                    ID = 16,
                    MenuID = 9,
                    RoleID = 2

                },
                 new RoleHasMenu
                 {
                     ID = 17,
                     MenuID = 7,
                     RoleID = 3

                 }
            );
            */


        }


        public DbSet<UserAccount> UserAccounts { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<RoleHasMenu> RoleHasMenu { get; set; }
        public DbSet<Admin> Admin { get; set; }
        public DbSet<Customer> Customer { get; set; }
       // public DbSet<ControlAcceso.Dtos.AdminDto> AdminDto { get; set; }
        public DbSet<Booking> Booking { get; set; }
        public DbSet<Horse> Horse { get; set; }
        public DbSet<CaballosAGP.Models.HorseClass> HorseClass { get; set; }
        public DbSet<CaballosAGP.Models.HorseSaddle> HorseSaddle { get; set; }
        public DbSet<CaballosAGP.Models.RaceTrack> RaceTrack { get; set; }
        public DbSet<CaballosAGP.Models.Teacher> Teacher { get; set; }
       
       // public DbSet<ControlAcceso.Dtos.TeacherDto> TeacherDto { get; set; }
       // public DbSet<ControlAcceso.Dtos.CustomerDto> CustomerDto { get; set; }

    }
}
