﻿using CaballosAGP.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaballosAGP.Filtros
{
    public class FilterAdmin : IActionFilter
    {
        private readonly ControlContext _context;

        public FilterAdmin(ControlContext context)
        {
            _context = context;
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
            //throw new NotImplementedException();
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var user = _context.UserAccounts.Where(u => u.ID == Convert.ToInt32(context.HttpContext.Session.GetString("usuario"))).FirstOrDefault();
            //if(user==null) si no existe el usuario
            if (user == null)
            {
                context.Result = new RedirectToActionResult("index", "login", null);
            }
            //Obtenemos el rol del usuario
            var role = _context.Role.Where(r => r.ID == user.RoleID).FirstOrDefault();
            
            //Si está vacío lo devuelvo al login
            if(role == null)
            {
                context.Result = new RedirectToActionResult("index", "login", null);

            }
            //Tambien se puede ver por id, si es distinto de 1 no es Admin
            if(role.RoleName != "Admin")
            {
                context.Result = new RedirectToActionResult("index", "Home", null);
            }

        }
    }
}
