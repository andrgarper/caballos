﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CaballosAGP.Data;
using CaballosAGP.Models;
using Microsoft.AspNetCore.Http;

namespace CaballosAGP.Controllers
{
    
    public class HorsesController : Controller
    {
        private readonly ControlContext _context;

        public HorsesController(ControlContext context)
        {
            _context = context;
        }

        // GET: Horses
        public async Task<IActionResult> Index()
        {
            var controlContext = _context.Horse.Include(h => h.HorseSaddle);
            return View(await controlContext.ToListAsync());
        }

        // GET: Horses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horse = await _context.Horse
                .Include(h => h.HorseSaddle)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (horse == null)
            {
                return NotFound();
            }

            return View(horse);
        }

        // GET: Horses/Create
        public IActionResult Create()
        {
            ViewData["HorseSaddleID"] = new SelectList(_context.Set<HorseSaddle>(), "ID", "Condition");
            return View();
        }

        // POST: Horses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Race,Birthday,Colour,Level,HorseSaddleID")] Horse horse, IFormFile img)
        {
            var montura = _context.HorseSaddle.Where(h => h.ID == horse.HorseSaddleID).FirstOrDefault();

            if (montura == null)
            {
                return RedirectToAction("Create", "Horses", new { errorMessage = "Horse saddle not found" });
            }

            if(img != null)
            {
                horse.Image = Util.GetByteArrayFromImage(img);
                horse.ImageSourceFileName = System.IO.Path.GetFileName(img.FileName);
                horse.ImageContentType = img.ContentType;
            }
            else

            {
                TempData["errorMessage"] = "La imagen es null";
                return RedirectToAction("Create", "Horses");
            }

            if (ModelState.IsValid)
            {
                _context.Add(horse);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["HorseSaddleID"] = new SelectList(_context.Set<HorseSaddle>(), "ID", "Condition", horse.HorseSaddleID);
            return View(horse);
        }

        // GET: Horses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horse = await _context.Horse.FindAsync(id);
            if (horse == null)
            {
                return NotFound();
            }
            ViewData["HorseSaddleID"] = new SelectList(_context.Set<HorseSaddle>(), "ID", "Condition", horse.HorseSaddleID);
            return View(horse);
        }

        // POST: Horses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Race,Birthday,Colour,Level,HorseSaddleID")] Horse horse, IFormFile img)
        {
            if (id != horse.ID)
            {
                return NotFound();
            }

            var montura = _context.HorseSaddle.Where(h => h.ID == horse.HorseSaddleID).FirstOrDefault();

            if (montura == null)
            {
                return RedirectToAction("Create", "Horses", new { errorMessage = "Horse saddle not found" });
            }

            if (img != null)
            {
                horse.Image = Util.GetByteArrayFromImage(img);
                horse.ImageSourceFileName = System.IO.Path.GetFileName(img.FileName);
                horse.ImageContentType = img.ContentType;
            }
            else

            {
                TempData["errorMessage"] = "La imagen es null";
                return RedirectToAction("Create", "Horses");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(horse);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HorseExists(horse.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["HorseSaddleID"] = new SelectList(_context.Set<HorseSaddle>(), "ID", "Condition", horse.HorseSaddleID);
            return View(horse);
        }

        // GET: Horses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horse = await _context.Horse
                .Include(h => h.HorseSaddle)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (horse == null)
            {
                return NotFound();
            }

            return View(horse);
        }

        // POST: Horses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var horse = await _context.Horse.FindAsync(id);
            var horseSaddle = _context.HorseSaddle.Where(hs => hs.ID.Equals(horse.HorseSaddleID)).FirstOrDefault();
            var booking = _context.Booking.Where(b => b.HorseID.Equals(id)).ToList();
            foreach (Booking b in booking)
            {
                _context.Booking.Remove(b);
            }
            horseSaddle.Reserved = false;
            _context.HorseSaddle.Update(horseSaddle);
           // var roleAdminID = _context.Role.Where(r => r.Name.Equals("Admin")).First();
            var userAccountlist = _context.UserAccounts.Where(u => u.RoleID.Equals(4)).ToList();
            foreach (UserAccount u in userAccountlist)
            {
                Util.SendEmail(u.Email, u.Username, "Prueba de email de eliminacion de caballo");

            }
            _context.Horse.Remove(horse);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HorseExists(int id)
        {
            return _context.Horse.Any(e => e.ID == id);
        }
    }
}
