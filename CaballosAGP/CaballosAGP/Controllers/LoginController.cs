﻿using CaballosAGP.Data;
using CaballosAGP.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace CaballosAGP.Controllers
{
    public class LoginController : Controller
    {

        private readonly ControlContext _context;

        public LoginController(ControlContext context)
        {
            _context = context;
        }

        //Get de Login
        public IActionResult Index()
        {
            return View();
        }
        //Post
        public IActionResult Login(string username, string password) {

            //convertimos la password en encriptar
            string passwordByte = Util.Encriptar(password);
            Debug.WriteLine(username + "------------------------------- " + password);                                                                      //Comparamos la pass encriptada
            var usuario = _context.UserAccounts.Where(u => u.Username == username && u.Password == passwordByte).FirstOrDefault();
           
            if (usuario != null)
            {
                //Guardamos en la Session el id y el username para mostrarlo como ejemplo en la vista
                HttpContext.Session.SetString("usuario", usuario.ID.ToString());
                HttpContext.Session.SetString("username", usuario.Username.ToString());
                //Guardamos en Session el rol para La funcionalidad 8
                var rol = _context.Role.Where(r => r.ID.Equals(usuario.RoleID)).FirstOrDefault();
                HttpContext.Session.SetString("rol", rol.RoleName.ToString());

                //Cargamos el menu correspondiente

                Util.Menus = _context.RoleHasMenu.Include(m => m.Menu).Where(m => m.RoleID == usuario.RoleID).Select(m => m.Menu).ToList();
            }
            else
            {
                TempData["ErrorMessage"] = "No se ha encontrado el usuario, inténtelo de nuevo.";
                return RedirectToAction(nameof(Index));
            }
            
            return RedirectToAction("Index","Home");

          }

        public IActionResult Logout()
        {
            try { 
                HttpContext.Session.Remove("usuario");
                HttpContext.Session.Remove("username");
                HttpContext.Session.Remove("rol");
                //Vaciamos la lista de menu para que no se vuelva a mostrar
                Util.Menus = new List<Menu>();
                return RedirectToAction("Index", "Login");
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return RedirectToAction("Index", "Home");
            }
            
        }
    }
}
