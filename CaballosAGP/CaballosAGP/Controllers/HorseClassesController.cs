﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CaballosAGP.Data;
using CaballosAGP.Models;

namespace CaballosAGP.Controllers
{
   // [ServiceFilter(typeof(FiltroSession))]
    public class HorseClassesController : Controller
    {
        private readonly ControlContext _context;

        public HorseClassesController(ControlContext context)
        {
            _context = context;
        }
        
        
        // GET: HorseClasses
        public async Task<IActionResult> Index()
        {
            var controlContext = _context.HorseClass.Include(h => h.RaceTrack).Include(h => h.Teacher);
            return View(await controlContext.ToListAsync());
        }

        // GET: HorseClasses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horseClass = await _context.HorseClass
                .Include(h => h.RaceTrack)
                .Include(h => h.Teacher)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (horseClass == null)
            {
                return NotFound();
            }

            return View(horseClass);
        }

        // GET: HorseClasses/Create
        public IActionResult Create()
        {
            ViewData["RaceTrackID"] = new SelectList(_context.Set<RaceTrack>(), "ID", "Name");
            ViewData["TeacherID"] = new SelectList(_context.Set<Teacher>(), "ID", "Name");
            return View();
        }

        // POST: HorseClasses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Level,Hour,Date,Assistants,Duration,TeacherID,RaceTrackID")] HorseClass horseClass)
        {
            //FUNCIONALIDAD 3: No crea clase si la pista está ocupada en el horario establecido.
            //Comprobamos si nos devuelve una clase en base de datos que esté en la misma hora y fecha en la que se ha creado
            var classhorse = _context.HorseClass.Where(h => h.Date == horseClass.Date && h.Hour==horseClass.Hour).FirstOrDefault();

            if(classhorse != null){
                TempData["errorMessage"] = "No puedes crear una clase porque ya hay una clase a esa hora";
                //Redirigimos al create
                return RedirectToAction(nameof(Create));

            }

            if (ModelState.IsValid)
            {
                _context.Add(horseClass);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["RaceTrackID"] = new SelectList(_context.Set<RaceTrack>(), "ID", "Name", horseClass.RaceTrackID);
            ViewData["TeacherID"] = new SelectList(_context.Set<Teacher>(), "ID", "Name", horseClass.TeacherID);
            return View(horseClass);
        }

        // GET: HorseClasses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horseClass = await _context.HorseClass.FindAsync(id);
            if (horseClass == null)
            {
                return NotFound();
            }
            ViewData["RaceTrackID"] = new SelectList(_context.Set<RaceTrack>(), "ID", "Name", horseClass.RaceTrackID);
            ViewData["TeacherID"] = new SelectList(_context.Set<Teacher>(), "ID", "Name", horseClass.TeacherID);
            return View(horseClass);
        }

        // POST: HorseClasses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Level,Hour,Date,Assistants,Duration,TeacherID,RaceTrackID")] HorseClass horseClass)
        {
            if (id != horseClass.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(horseClass);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HorseClassExists(horseClass.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["RaceTrackID"] = new SelectList(_context.Set<RaceTrack>(), "ID", "Name", horseClass.RaceTrackID);
            ViewData["TeacherID"] = new SelectList(_context.Set<Teacher>(), "ID", "Name", horseClass.TeacherID);
            return View(horseClass);
        }

        // GET: HorseClasses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horseClass = await _context.HorseClass
                .Include(h => h.RaceTrack)
                .Include(h => h.Teacher)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (horseClass == null)
            {
                return NotFound();
            }

          

            //FUNCIONALIDAD 12:No se puede eliminar una clase si ya tiene reservas.
            var listBookings = _context.Booking.Where(b => b.HorseClassID.Equals(id)).ToList();
            if (listBookings != null)
            {
                TempData["errorMessage"] = "No puedes borrar una clase que esté reservada";
                //Redirigimos al create
                return RedirectToAction(nameof(Index));
            }

            return View(horseClass);
        }

        // POST: HorseClasses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            

            var horseClass = await _context.HorseClass.FindAsync(id);         
            _context.HorseClass.Remove(horseClass);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HorseClassExists(int id)
        {
            return _context.HorseClass.Any(e => e.ID == id);
        }
    }
}
