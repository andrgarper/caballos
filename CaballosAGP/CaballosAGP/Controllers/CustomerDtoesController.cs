﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CaballosAGP.Data;
using CaballosAGP.Dtos;
using CaballosAGP.Models;

namespace CaballosAGP.Controllers
{
   // (([ServiceFilter(typeof(FiltroSession))]
    public class CustomerDtoesController : Controller
    {
        private readonly ControlContext _context;

        public CustomerDtoesController(ControlContext context)
        {
            _context = context;
        }    
      
        // GET: CustomerDtoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CustomerDtoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Username,Password,Email,Active,Name,Surname,Phone,Age,Birthday,DNI,Address,City,PostalCode,Level")] CustomerDto customerDto)
        {
            Role role = _context.Role.Where(r => r.RoleName == "Customer").FirstOrDefault();
            if (ModelState.IsValid)
            {

                //Creamos los objetos que queremos guardar en base de datos
                UserAccount userAccount = new UserAccount();
                Customer customer = new Customer();

                //Seteamos estos objetos
                userAccount.Username = customerDto.Username;
                userAccount.Password = Util.Encriptar(customerDto.Password);
                userAccount.Email = customerDto.Email;
                userAccount.RoleID = role.ID;
                userAccount.Active = customerDto.Active;

                _context.Add(userAccount);
                await _context.SaveChangesAsync();

                customer.Name = customerDto.Name;
                customer.Surname = customerDto.Surname;
                customer.Phone = customerDto.Phone;
                customer.Age = customerDto.Age;
                customer.UserAccountID = userAccount.ID;
                customer.Birthday = customerDto.Birthday;
                customer.DNI = customerDto.DNI;
                customer.Address = customerDto.Address;
                customer.City = customerDto.City;
                customer.PostalCode = customerDto.PostalCode;
                customer.Level = customerDto.Level;
                _context.Add(customer);

                await _context.SaveChangesAsync();
                Util.SendEmail("csi21-abormez@colegioaltair.net", customerDto.Name, "Prueba de email");
                return RedirectToAction("Index", "Login");
            }
            return View(customerDto);
        }

      
       
        
    }
}
