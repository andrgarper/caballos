﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CaballosAGP.Data;
using CaballosAGP.Dtos;
using CaballosAGP.Models;

namespace CaballosAGP.Controllers
{
    public class TeacherDtoesController : Controller
    {
        private readonly ControlContext _context;

        public TeacherDtoesController(ControlContext context)
        {
            _context = context;
        }

       

        // GET: TeacherDtoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TeacherDtoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Username,Password,Email,Active,Name,Surname,Phone,Age")] TeacherDto teacherDto)
        {
            Role role = _context.Role.Where(r => r.RoleName == "Teacher").FirstOrDefault();
            if (ModelState.IsValid)
            {
                //Creo los objetos
                Teacher teacher = new Teacher();
                UserAccount userAccount = new UserAccount();

                //Seteamos
                userAccount.Username = teacherDto.Username;
                userAccount.Password = Util.Encriptar(teacherDto.Password);
                userAccount.Email = teacherDto.Email;
                userAccount.RoleID = role.ID;
                userAccount.Active = teacherDto.Active;

                _context.Add(userAccount);
                await _context.SaveChangesAsync();

                teacher.Name = teacherDto.Name;
                teacher.Surname = teacherDto.Surname;
                teacher.Phone = teacherDto.Phone;
                teacher.Age = teacherDto.Age;
                teacher.UserAccountID = userAccount.ID;
                _context.Add(teacher);

                
                await _context.SaveChangesAsync();
                
               //var roleAdminID = _context.Role.Where(r => r.RoleName.Equals("admin2")).First();
                var userAccountlist = _context.UserAccounts.Where(u => u.ID.Equals(4)).ToList();
                foreach (UserAccount u in userAccountlist)
                {
                    Util.SendEmail(u.Email, teacherDto.Name, "Prueba de email de creacion de teacher");

                }
                return RedirectToAction("Index", "Login");
            }
            return View(teacherDto);
        }

      

    }
}
