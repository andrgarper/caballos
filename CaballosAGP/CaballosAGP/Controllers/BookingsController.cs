﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CaballosAGP.Data;
using CaballosAGP.Models;

namespace CaballosAGP.Controllers
{
    
    public class BookingsController : Controller
    {
        private readonly ControlContext _context;

        public BookingsController(ControlContext context)
        {
            _context = context;
        }

        // GET: Bookings
        public async Task<IActionResult> Index()
        {
            var controlContext = _context.Booking.Include(b => b.Customer).Include(b => b.Horse).Include(b => b.HorseClass);
            return View(await controlContext.ToListAsync());
        }

        // GET: Bookings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking
                .Include(b => b.Customer)
                .Include(b => b.Horse)
                .Include(b => b.HorseClass)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (booking == null)
            {
                return NotFound();
            }

            return View(booking);
        }

        // GET: Bookings/Create
        public IActionResult Create()
        {
            ViewData["CustomerID"] = new SelectList(_context.Customer, "ID", "Name");
            ViewData["HorseID"] = new SelectList(_context.Set<Horse>(), "ID", "Name");
            ViewData["HorseClassID"] = new SelectList(_context.Set<HorseClass>(), "ID", "Name");
            return View();
        }

        // POST: Bookings/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,BookingDate,CustomerID,HorseID,HorseClassID")] Booking booking)

          
        {   //Recuperamos cada objeto para ver la condición
            var customer = _context.Customer.Where(c => c.ID == booking.CustomerID).FirstOrDefault();
            var horse = _context.Horse.Where(h => h.ID == booking.HorseID).FirstOrDefault();
            var horseClass = _context.HorseClass.Where(h => h.ID == booking.HorseClassID).FirstOrDefault();
            var raceTrack = _context.RaceTrack.Where(r => r.ID == horseClass.RaceTrackID).FirstOrDefault();


            //FUNCIONALIDAD 1: El mismo usuario no puede darse de alta dos veces en la misma clase  
            //Recogemos el objeto de la tabla bookings donde sea igual el cliente, la clase y la fecha
            var funcional1 = _context.Booking.Where(b => b.CustomerID.Equals(booking.CustomerID)
            && b.HorseClassID.Equals(booking.HorseClassID)
            && b.BookingDate.Equals(booking.BookingDate)).FirstOrDefault();

            if (funcional1 != null)//Se ha encontrado ya una reserva, con un cliente, clase y fecha iguales a las que el usuario quiere crear.
            {
                //Como no puede darse de alta dos veces se mando un error y lo redirijo a la misma vista
                TempData["ErrorMessage"] = "El usuario ya tiene reservada una clase y no puede volver a reservarla";
                return RedirectToAction(nameof(Create));
            }

            //FUNCIONALIDAD 2: No realiza reserva a una clase si se pisa con otra que ya ha sido reservada.

            //Recuperamos una reserva donde sea la misma fecha y pista que la que se está intentado crear
            var funcional2 = _context.Booking.Where(b => b.BookingDate.Equals(booking.BookingDate)
             && horseClass.RaceTrackID.Equals(raceTrack.ID)).FirstOrDefault();

            if (funcional2 != null)
            {
                TempData["ErrorMessage"] = "No se puede reservar una clase que tiene una ya usandose y en la misma fecha que otra.";
                return RedirectToAction(nameof(Create));
            }
            //FUNCIONALIDAD 4: Un cliente solo puede reservar caballo y clases de su nivel.
            //Si el nivel de customer no son el mismo nivel con el caballo y la clase no se podrá crear la reserva
            if (!(customer.Level==horse.Level && customer.Level==horseClass.Level)) //CORRECTA
              {

                  TempData["errorMessage"] = "customer, horse y horseClass deben ser del mismo nivel";
                  //Redirigimos al create
                  return RedirectToAction(nameof(Create));
              }

            //FUNCIONALIDAD 7:
            //Caballos para alquilar -> libres en día y hora
            var listBookings = _context.Booking.Where(b => b.BookingDate == booking.BookingDate && b.HorseID == booking.HorseID).FirstOrDefault();
            if (listBookings != null) //CORRECTA
            {
                TempData["errorMessage"] = "El caballo no está disponible, porque hay otra reserva para dicho caballo";
                //Redirigimos al create
                return RedirectToAction(nameof(Create));
            }

            //FUNCIONALIDAD 8:Aforo completo en una clase no deja reservar.
            var listBookings2 = _context.Booking.Where(b => b.HorseClassID.Equals(booking.HorseClassID)).ToList();

            if (listBookings2.Count >= horseClass.Assistants)
            {
                TempData["ErrorMessage"] = "Aforo maximo completado.";
                return RedirectToAction(nameof(Create));
            }

            if (ModelState.IsValid)
            {
                _context.Add(booking);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CustomerID"] = new SelectList(_context.Customer, "ID", "Name", booking.CustomerID);
            ViewData["HorseID"] = new SelectList(_context.Set<Horse>(), "ID", "Name", booking.HorseID);
            ViewData["HorseClassID"] = new SelectList(_context.Set<HorseClass>(), "ID", "Name", booking.HorseClassID);
            return View(booking);
        }

        // GET: Bookings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking.FindAsync(id);
            if (booking == null)
            {
                return NotFound();
            }
            ViewData["CustomerID"] = new SelectList(_context.Customer, "ID", "Name", booking.CustomerID);
            ViewData["HorseID"] = new SelectList(_context.Set<Horse>(), "ID", "Name", booking.HorseID);
            ViewData["HorseClassID"] = new SelectList(_context.Set<HorseClass>(), "ID", "Name", booking.HorseClassID);
            return View(booking);
        }

        // POST: Bookings/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,BookingDate,CustomerID,HorseID,HorseClassID")] Booking booking)
        {
            if (id != booking.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(booking);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingExists(booking.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CustomerID"] = new SelectList(_context.Customer, "ID", "Name", booking.CustomerID);
            ViewData["HorseID"] = new SelectList(_context.Set<Horse>(), "ID", "Name", booking.HorseID);
            ViewData["HorseClassID"] = new SelectList(_context.Set<HorseClass>(), "ID", "Name", booking.HorseClassID);
            return View(booking);
        }

        // GET: Bookings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Booking
                .Include(b => b.Customer)
                .Include(b => b.Horse)
                .Include(b => b.HorseClass)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (booking == null)
            {
                return NotFound();
            }

            return View(booking);
        }

        // POST: Bookings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var booking = await _context.Booking.FindAsync(id);
            _context.Booking.Remove(booking);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookingExists(int id)
        {
            return _context.Booking.Any(e => e.ID == id);
        }
    }
}
