﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CaballosAGP.Models
{
    public class Teacher : Person
    {

        [InverseProperty("Teacher")]
        public List<HorseClass> ListHorseClass { get; set; }
    }
}
