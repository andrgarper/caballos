﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CaballosAGP.Models
{
    public class Menu
    {
        public int ID { get; set; }    
        public string Controller { get; set; }
       
        public string Action { get; set; }
        
        public string Label { get; set; }

        [InverseProperty("Menu")]
        public  List<RoleHasMenu> ListRoleHasMenu { get; set; }

    }
}
