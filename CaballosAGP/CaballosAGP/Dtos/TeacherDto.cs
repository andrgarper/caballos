﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CaballosAGP.Dtos
{
    public class TeacherDto
    {
        public int ID { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "el nombre de usuario debe ser entre 5 y 20")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public bool Active { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        [StringLength(20, ErrorMessage = "el nombre debe ser como mínimo 20 characters")]
        public string Name { get; set; }

        [Display(Name = "Apellidos")]
        [Required]
        [StringLength(20, ErrorMessage = "el apellido debe ser como mínimo 20 characters")]
        public string Surname { get; set; }

        [Required]
        public string Phone { get; set; }
        [Range(18, 65, ErrorMessage = "Debes ser mayor de edad")]
        public int Age { get; set; }
    }
}
